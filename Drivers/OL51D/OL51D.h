/* ::::::::::::::::::::::::::::::::::::
    OL51D uDisplay Configuration
   :::::::::::::::::::::::::::::::::::: */
#include "stm32l0xx_hal.h"
#include "stdbool.h"

enum LUM_MODE{
        LUM_LOW,	
        LUM_MED,
	      LUM_HIGH,
          };	

#define EEPROM_READ          0x03  /*!< Read from Memory Array */
#define EEPROM_WRITE         0x02  /*!< Write to Memory Array */

#define BITEK_1E5_VD_INFO_O                     0x1E5
#define BITEK_MASK_VD_READY_O                   0x01
#define BITEK_MASK_VD_HLCK_O                    0x02    
#define BITEK_MASK_SYNC_READY_O                 0x04
#define BITEK_MASK_STD_READY_O                  0x08
#define BITEK_MASK_VD_STDCHG_O                  0x10
#define BITEK_MASK_AUTO_VTRC_O                  0x20    

#define BITEK_1E6_STD_INFO_O                    0x1E6
#define BITEK_MASK_STD_MODE_O                   0x07
#define BITEK_MASK_FIDT_O                       0x08    // 0=50Hz, 1=60Hz

void OL51D_Init(void);
void OL51D_Reset(void); 
void OL51D_POWERON(void);
void OL51D_POWEROFF(void); 
void OL51D_PowerSave(void); 

_Bool uDISPLAY_EEPROM_READ(void);
_Bool EEPROM_SPI_ReadBuffer(uint8_t* pBuffer, uint16_t ReadAddr, uint16_t NumByteToRead);

uint8_t SPI_ReadRegister_8b(uint8_t reg_add);
uint8_t SPI_ReadRegister_8b_0(uint8_t reg_add);
uint8_t SPI_ReadRegister_8b_2(uint8_t reg_add);
uint8_t SPI_ReadRegister_8b_3(uint8_t reg_add);
bool SPI_ReadRegister_Burst(uint16_t reg_add, uint8_t *read_data,uint8_t len); 
bool SPI_WriteRegister_8b(uint8_t reg_add,uint8_t value);
bool SPI_WriteRegister_Burst(uint8_t reg_add,uint8_t *array,uint8_t len);
void uDisplay_SerialSettings(uint8_t order);
void uDisplay_SerialSettings_2(uint8_t order);
void uDisplay_LuminanceMode(uint8_t mode);
void Input_Video_Check(void);

